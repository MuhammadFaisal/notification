package com.example.faisal.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Faisal on 23/02/2019.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private ArrayList<Notification> contents;

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView content,time;
        CircleImageView profile;

        public MyViewHolder(View v){
            super(v);
            this.content = (TextView) v.findViewById(R.id.content);
            this.time = (TextView) v.findViewById(R.id.time);
            this.profile = (CircleImageView) v.findViewById(R.id.profile_image);

        }
    }

    public NotificationAdapter(ArrayList<Notification> data){
        this.contents = data;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder,final int position) {
        TextView content = holder.content;
        TextView time = holder.time;
        ImageView profile = holder.profile;

        content.setText(contents.get(position).getContent());
        time.setText(contents.get(position).getTime());
        profile.setImageResource(contents.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return contents.size();
    }
}

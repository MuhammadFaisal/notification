package com.example.faisal.myapplication;

/**
 * Created by Faisal on 23/02/2019.
 */

public class Bisnis {
    String name;
    String investor;
    int profile;
    int thropy;

    public Bisnis(String name, String investor, int profile, int thropy){
        this.name = name;
        this.investor = investor;
        this.profile = profile;
        this.thropy = thropy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInvestor() {
        return investor;
    }

    public void setInvestor(String investor) {
        this.investor = investor;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public int getThropy() {
        return thropy;
    }

    public void setThropy(int thropy) {
        this.thropy = thropy;
    }
}

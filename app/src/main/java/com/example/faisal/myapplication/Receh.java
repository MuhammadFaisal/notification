package com.example.faisal.myapplication;

/**
 * Created by Faisal on 23/02/2019.
 */

public class Receh {
    String nama;
    String receh;
    int profile;
    int thropy;

    public Receh(String nama, String receh, int profile, int thropy){
        this.nama = nama;
        this.receh = receh;
        this.profile = profile;
        this.thropy = thropy;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getReceh() {
        return receh;
    }

    public void setReceh(String receh) {
        this.receh = receh;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public int getThropy() {
        return thropy;
    }

    public void setThropy(int thropy) {
        this.thropy = thropy;
    }
}

package com.example.faisal.myapplication;

/**
 * Created by Faisal on 23/02/2019.
 */

public class Notification {

    String content;
    String time;
    int image;

    public Notification(String content, String time, int image){
        this.content = content;
        this.time = time;
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

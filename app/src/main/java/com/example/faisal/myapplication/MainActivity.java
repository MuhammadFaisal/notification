package com.example.faisal.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView rv_notification;
    private static ArrayList<Notification> notifications;
    static View.OnClickListener myOnClickListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myOnClickListener = new MyOnClickListener(this);

        rv_notification = findViewById(R.id.rv_notification);
        rv_notification.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        rv_notification.setLayoutManager(layoutManager);
        rv_notification.setItemAnimator(new DefaultItemAnimator());

        notifications = new ArrayList<Notification>();
        for (int i =0; i < Content.notification.length; i++){
            notifications.add(new Notification(
                    Content.notification[i],
                    Content.time[i],
                    Content.profile[i]
            ));
        }
        adapter = new NotificationAdapter(notifications);
        rv_notification.setAdapter(adapter);
    }

    private static class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(context, "LO SIAPA AJG!", Toast.LENGTH_SHORT).show();
        }
    }
}

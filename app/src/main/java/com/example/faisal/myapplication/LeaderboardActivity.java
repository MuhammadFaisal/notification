package com.example.faisal.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class LeaderboardActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter,adapters;
    private RecyclerView.LayoutManager layoutManager,layoutManager2;
    private static RecyclerView rv_leaderboard_1,rv_leaderboard_2;
    private static ArrayList<Receh> recehs;
    private static ArrayList<Bisnis> bisnis;
    static View.OnClickListener myOnClickListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        myOnClickListener = new MyOnClickListener(this);

        rv_leaderboard_1 = findViewById(R.id.rv_leaderboard_1);
        rv_leaderboard_1.setHasFixedSize(true);

        rv_leaderboard_2 = findViewById(R.id.rv_leaderboard_2);
        rv_leaderboard_2.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        layoutManager2 = new LinearLayoutManager(this);

        rv_leaderboard_1.setLayoutManager(layoutManager);
        rv_leaderboard_1.setItemAnimator(new DefaultItemAnimator());

        rv_leaderboard_2.setLayoutManager(layoutManager2);
        rv_leaderboard_2.setItemAnimator(new DefaultItemAnimator());

        recehs = new ArrayList<Receh>();
        for (int i =0; i < RecehTertinggi.nama.length; i++){
            recehs.add(new Receh(
                    RecehTertinggi.nama[i],
                    RecehTertinggi.receh[i],
                    RecehTertinggi.profile[i],
                    RecehTertinggi.thropy[i]
            ));
        }

        bisnis = new ArrayList<Bisnis>();
        for (int i=0; i < BisnisTerfavorit.bisnis.length; i++){
            bisnis.add(new Bisnis(
                    BisnisTerfavorit.bisnis[i],
                    BisnisTerfavorit.investor[i],
                    BisnisTerfavorit.profile[i],
                    BisnisTerfavorit.thropy[i]
            ));
        }
        adapter = new RecehAdapter(recehs);
        adapters = new BisnisAdapter(bisnis);

        rv_leaderboard_1.setAdapter(adapter);
        rv_leaderboard_2.setAdapter(adapters);
    }

    private static class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(context, "LO SIAPA AJG!", Toast.LENGTH_SHORT).show();
        }
    }
}

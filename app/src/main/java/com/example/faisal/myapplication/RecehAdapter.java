package com.example.faisal.myapplication;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Faisal on 23/02/2019.
 */

public class RecehAdapter extends RecyclerView.Adapter<RecehAdapter.MyViewHolder> {

    private ArrayList<Receh> recehs;

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nama,receh;
        CircleImageView profile;
        ImageView thropy;

        public MyViewHolder(View v){
            super(v);
            this.nama = (TextView) v.findViewById(R.id.nama);
            this.receh = (TextView) v.findViewById(R.id.receh);
            this.profile = (CircleImageView) v.findViewById(R.id.profile);
            this.thropy = (ImageView) v.findViewById(R.id.thropy);

        }
    }

    public RecehAdapter(ArrayList<Receh> data){
        this.recehs = data;
    }

    @Override
    public RecehAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_1,parent,false);
        view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextView nama = holder.nama;
        TextView receh = holder.receh;
        CircleImageView profile = holder.profile;
        ImageView thropy = holder.thropy;

        nama.setText(recehs.get(position).getNama());
        receh.setText(recehs.get(position).getReceh());
        profile.setImageResource(recehs.get(position).getProfile());
        thropy.setImageResource(recehs.get(position).getThropy());

    }

    @Override
    public int getItemCount() {
        return recehs.size();
    }
}

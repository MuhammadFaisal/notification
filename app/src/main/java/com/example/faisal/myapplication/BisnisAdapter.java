package com.example.faisal.myapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Faisal on 23/02/2019.
 */

public class BisnisAdapter extends RecyclerView.Adapter<BisnisAdapter.MyViewHolder> {

    private ArrayList<Bisnis> bisnis;

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nama,investor;
        CircleImageView profile;
        ImageView thropy;

        public MyViewHolder(View v){
            super(v);
            this.nama = (TextView) v.findViewById(R.id.nama);
            this.investor = (TextView) v.findViewById(R.id.investor);
            this.profile = (CircleImageView) v.findViewById(R.id.profile);
            this.thropy = (ImageView) v.findViewById(R.id.thropy);

        }
    }

    public BisnisAdapter(ArrayList<Bisnis> data){
        this.bisnis = data;
    }

    @Override
    public BisnisAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_2,parent,false);
        view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextView nama = holder.nama;
        TextView investor = holder.investor;
        CircleImageView profile = holder.profile;
        ImageView thropy = holder.thropy;

        nama.setText(bisnis.get(position).getName());
        investor.setText(bisnis.get(position).getInvestor());
        profile.setImageResource(bisnis.get(position).getProfile());
        thropy.setImageResource(bisnis.get(position).getThropy());

    }

    @Override
    public int getItemCount() {
        return bisnis.size();
    }
}

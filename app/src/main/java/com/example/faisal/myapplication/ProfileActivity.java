package com.example.faisal.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout linearEdit,linearLeaderboard,linearAbout,linearLogout,linearInvite,linearTutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        linearEdit = findViewById(R.id.linearEdit);
        linearLeaderboard = findViewById(R.id.linearLeaderboard);
        linearAbout = findViewById(R.id.linearAbout);
        linearLogout = findViewById(R.id.linearLogout);
        linearInvite = findViewById(R.id.linearInvite);
        linearTutorial = findViewById(R.id.linearTutorial);

        linearLeaderboard.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == linearLeaderboard){
            Intent i = new Intent(ProfileActivity.this,LeaderboardActivity.class);
            startActivity(i);
            finish();
        }
    }
}
